var config = {
    apiKey: "AIzaSyD4YjO1ovXfsK3yYTaf7f5ieLSPIPNaWlE",
    authDomain: "cdejavascriptchallenge.firebaseapp.com",
    databaseURL: "https://cdejavascriptchallenge.firebaseio.com",
    projectId: "cdejavascriptchallenge",
    storageBucket: "",
    messagingSenderId: "387600950021"
  };
firebase.initializeApp(config);
var database = firebase.database();

if (window.location.hash=='#home') {
	document.getElementById('Order').style.display='none';
	document.getElementById('News').style.display='none';
} else {
	document.getElementById('News').style.display='none';
	document.getElementById('Home').style.display='none';
}

document.getElementById('homeBtn').onclick=function() {
    document.getElementById('Order').style.display = 'none';
    document.getElementById('News').style.display = 'none';
    document.getElementById('Home').style.display = 'block';
}

document.getElementById('newsBtn').onclick=function() {
    document.getElementById('Order').style.display = 'none';
    document.getElementById('Home').style.display = 'none';
    document.getElementById('News').style.display = 'block';
}

document.getElementById('orderBtn').onclick=function() {
    document.getElementById('News').style.display = 'none';
    document.getElementById('Home').style.display = 'none';
    document.getElementById('Order').style.display = 'block';
}

document.getElementById('submit').onclick=function() {
	var email = document.getElementById('email').value.toString();
	var order = document.getElementById('order').value.toString();
	if(email.length <= 0 || order.length <= 0){
	    if(email.length <= 0 && order.length <= 0){alert("Plz enter Email & Order");}
	    if(email.length <= 0 && order.length > 1){alert("Plz enter Email");}
	    if(email.length > 1 && order.length <= 1){alert("Plz enter Order");}
	} else {
        var checkEmail = /\S+@\S+\.\S+/;
        if(checkEmail.test(email)==false){
            alert("Plz check your Email");
        } else {
            firebase.database().ref('/' + Date()).set({
            email: email,
            order : order,
            time : Date()
            });
            alert("Thank You.");
        }
	}
};